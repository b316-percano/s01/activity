package com.zuitt.activity1;
import java.util.Scanner;

public class UserDetails {
    public static void main(String[] args) {
        // Firstname
        Scanner firstName = new Scanner(System.in);
        System.out.println("Enter your Firstname: ");
        String FirstName = firstName.nextLine();

        // Lastname
        Scanner lastName = new Scanner(System.in);
        System.out.println("Enter your Lastname: ");
        String LastName = lastName.nextLine();

        // First Subject
        Scanner firstSubject = new Scanner(System.in);
        System.out.println("First Subject Grade: ");
        double FirstSubj = new Double(firstSubject.nextLine());

        // Second Subject
        Scanner secondSubject = new Scanner(System.in);
        System.out.println("Second Subject Grade: ");
        double SecondSubj = new Double(secondSubject.nextLine());

        // Third Subject
        Scanner thirdSubject = new Scanner(System.in);
        System.out.println("Third Subject Grade: ");
        double ThirdSubj = new Double(thirdSubject.nextLine());

        // Total Average + message
        System.out.println("Good day, " + FirstName + " " + LastName + ".");
        System.out.println("Your grade average is: " + (FirstSubj + SecondSubj + ThirdSubj)/3);
    }
}
